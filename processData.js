function processData(myData) {

	
	var myValues1 = [];
	var myLabels = [];


	for (var i = 0 ; i < 32 ; i++) {
		myLabels[i] = myData[i].Years;
		myValues1[i] = myData[i].Degrees;

	}


	var myChart = new Chart($("#myChart"), {
		type: 'line',
		data: {
			labels: myLabels,
			datasets: [{
				data: myValues1,
				fill: false,
					borderColor: 'black',
					backgroundColor: 'white',
				label: "Global Landsurface Temperature(C)"
			}]
		},
			options: {
				title: {
					display: true,
					fontColor: 'black',
					text: 'Temperature changed from 1980 to 2011'
				}
			}
		});
}

$(document).ready(function() {
	$.getJSON("http://danycabrera.com/csc130/proxy.php?key=1NAq1kAIrAPS_nqdSOhoE6gYFxjSNVYhkJATOpFBnFz8&sheet=Sheet1", processData);
});
