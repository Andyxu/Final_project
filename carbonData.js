function displayFunction(){
	var stateName;
	stateName = document.getElementById("stateName").value;

	switch (stateName){
		case 'AL':
			function processData(data){
				var code1 = "<p>Category id: " + data.request.category_id + "</p>";
				code1 += "<p>Command: " + data.request.command + "</p>";
				code1 += "<p>Parent_category_id: " + data.category.parent_category_id + "</p>";
				code1 += "<p>Name: " + data.category.name + "</p>";
				var i;
				var code = "<tr><th>Series_id</th><th>Name</th><th>f</th><th>Units</th><th>Updated</th></tr>";
				for(i in data.category.childseries){
					code += "<tr>";
					code += "<td>" + data.category.childseries[i].series_id + "</td>";
					code += "<td>" + data.category.childseries[i].name + "</td>";
					code += "<td>" + data.category.childseries[i].f + "</td>";
					code += "<td>" + data.category.childseries[i].units + "</td>";
					code += "<td>" + data.category.childseries[i].updated + "</td>";
					code += "</tr>";
				}
				document.getElementById("myLine").innerHTML = code1;
				document.getElementById("myTable").innerHTML = code;
			}
		$(document).ready(function() {
			$.getJSON("http://api.eia.gov/category/?api_key=8512eb9c1d86850c322cb9ed99052b73&category_id=2251672", processData);
		});
		break;

		case 'AK':
			function processData(data){
				var code1 = "<p>Category id: " + data.request.category_id + "</p>";
				code1 += "<p>Command: " + data.request.command + "</p>";
				code1 += "<p>Parent_category_id: " + data.category.parent_category_id + "</p>";
				code1 += "<p>Name: " + data.category.name + "</p>";
				var i;
				var code = "<tr><th>Series_id</th><th>Name</th><th>f</th><th>Units</th><th>Updated</th></tr>";
				for(i in data.category.childseries){
					code += "<tr>";
					code += "<td>" + data.category.childseries[i].series_id + "</td>";
					code += "<td>" + data.category.childseries[i].name + "</td>";
					code += "<td>" + data.category.childseries[i].f + "</td>";
					code += "<td>" + data.category.childseries[i].units + "</td>";
					code += "<td>" + data.category.childseries[i].updated + "</td>";
					code += "</tr>";
				}
				document.getElementById("myLine").innerHTML = code1;
				document.getElementById("myTable").innerHTML = code;
			}
		$(document).ready(function() {
			$.getJSON("http://api.eia.gov/category/?api_key=8512eb9c1d86850c322cb9ed99052b73&category_id=2251673", processData);
		});
		break;

		case 'AZ':
			function processData(data){
				var code1 = "<p>Category id: " + data.request.category_id + "</p>";
				code1 += "<p>Command: " + data.request.command + "</p>";
				code1 += "<p>Parent_category_id: " + data.category.parent_category_id + "</p>";
				code1 += "<p>Name: " + data.category.name + "</p>";
				var i;
				var code = "<tr><th>Series_id</th><th>Name</th><th>f</th><th>Units</th><th>Updated</th></tr>";
				for(i in data.category.childseries){
					code += "<tr>";
					code += "<td>" + data.category.childseries[i].series_id + "</td>";
					code += "<td>" + data.category.childseries[i].name + "</td>";
					code += "<td>" + data.category.childseries[i].f + "</td>";
					code += "<td>" + data.category.childseries[i].units + "</td>";
					code += "<td>" + data.category.childseries[i].updated + "</td>";
					code += "</tr>";
				}
				document.getElementById("myLine").innerHTML = code1;
				document.getElementById("myTable").innerHTML = code;
			}
		$(document).ready(function() {
			$.getJSON("http://api.eia.gov/category/?api_key=8512eb9c1d86850c322cb9ed99052b73&category_id=2251674", processData);
		});
		break;

		case 'AR':
			function processData(data){
				var code1 = "<p>Category id: " + data.request.category_id + "</p>";
				code1 += "<p>Command: " + data.request.command + "</p>";
				code1 += "<p>Parent_category_id: " + data.category.parent_category_id + "</p>";
				code1 += "<p>Name: " + data.category.name + "</p>";
				var i;
				var code = "<tr><th>Series_id</th><th>Name</th><th>f</th><th>Units</th><th>Updated</th></tr>";
				for(i in data.category.childseries){
					code += "<tr>";
					code += "<td>" + data.category.childseries[i].series_id + "</td>";
					code += "<td>" + data.category.childseries[i].name + "</td>";
					code += "<td>" + data.category.childseries[i].f + "</td>";
					code += "<td>" + data.category.childseries[i].units + "</td>";
					code += "<td>" + data.category.childseries[i].updated + "</td>";
					code += "</tr>";
				}
				document.getElementById("myLine").innerHTML = code1;
				document.getElementById("myTable").innerHTML = code;
			}
		$(document).ready(function() {
			$.getJSON("http://api.eia.gov/category/?api_key=8512eb9c1d86850c322cb9ed99052b73&category_id=2251675", processData);
		});
		break;


		case 'CA':
			function processData(data){
				var code1 = "<p>Category id: " + data.request.category_id + "</p>";
				code1 += "<p>Command: " + data.request.command + "</p>";
				code1 += "<p>Parent_category_id: " + data.category.parent_category_id + "</p>";
				code1 += "<p>Name: " + data.category.name + "</p>";
				var i;
				var code = "<tr><th>Series_id</th><th>Name</th><th>f</th><th>Units</th><th>Updated</th></tr>";
				for(i in data.category.childseries){
					code += "<tr>";
					code += "<td>" + data.category.childseries[i].series_id + "</td>";
					code += "<td>" + data.category.childseries[i].name + "</td>";
					code += "<td>" + data.category.childseries[i].f + "</td>";
					code += "<td>" + data.category.childseries[i].units + "</td>";
					code += "<td>" + data.category.childseries[i].updated + "</td>";
					code += "</tr>";
				}
				document.getElementById("myLine").innerHTML = code1;
				document.getElementById("myTable").innerHTML = code;
			}
		$(document).ready(function() {
			$.getJSON("http://api.eia.gov/category/?api_key=8512eb9c1d86850c322cb9ed99052b73&category_id=2251676", processData);
		});
		break;


		case 'CO':
			function processData(data){
				var code1 = "<p>Category id: " + data.request.category_id + "</p>";
				code1 += "<p>Command: " + data.request.command + "</p>";
				code1 += "<p>Parent_category_id: " + data.category.parent_category_id + "</p>";
				code1 += "<p>Name: " + data.category.name + "</p>";
				var i;
				var code = "<tr><th>Series_id</th><th>Name</th><th>f</th><th>Units</th><th>Updated</th></tr>";
				for(i in data.category.childseries){
					code += "<tr>";
					code += "<td>" + data.category.childseries[i].series_id + "</td>";
					code += "<td>" + data.category.childseries[i].name + "</td>";
					code += "<td>" + data.category.childseries[i].f + "</td>";
					code += "<td>" + data.category.childseries[i].units + "</td>";
					code += "<td>" + data.category.childseries[i].updated + "</td>";
					code += "</tr>";
				}
				document.getElementById("myLine").innerHTML = code1;
				document.getElementById("myTable").innerHTML = code;
			}
		$(document).ready(function() {
			$.getJSON("http://api.eia.gov/category/?api_key=8512eb9c1d86850c322cb9ed99052b73&category_id=2251677", processData);
		});
		break;




		case 'CT':
			function processData(data){
				var code1 = "<p>Category id: " + data.request.category_id + "</p>";
				code1 += "<p>Command: " + data.request.command + "</p>";
				code1 += "<p>Parent_category_id: " + data.category.parent_category_id + "</p>";
				code1 += "<p>Name: " + data.category.name + "</p>";
				var i;
				var code = "<tr><th>Series_id</th><th>Name</th><th>f</th><th>Units</th><th>Updated</th></tr>";
				for(i in data.category.childseries){
					code += "<tr>";
					code += "<td>" + data.category.childseries[i].series_id + "</td>";
					code += "<td>" + data.category.childseries[i].name + "</td>";
					code += "<td>" + data.category.childseries[i].f + "</td>";
					code += "<td>" + data.category.childseries[i].units + "</td>";
					code += "<td>" + data.category.childseries[i].updated + "</td>";
					code += "</tr>";
				}
				document.getElementById("myLine").innerHTML = code1;
				document.getElementById("myTable").innerHTML = code;
			}
		$(document).ready(function() {
			$.getJSON("http://api.eia.gov/category/?api_key=8512eb9c1d86850c322cb9ed99052b73&category_id=2251678", processData);
		});
		break;




		case 'DE':
			function processData(data){
				var code1 = "<p>Category id: " + data.request.category_id + "</p>";
				code1 += "<p>Command: " + data.request.command + "</p>";
				code1 += "<p>Parent_category_id: " + data.category.parent_category_id + "</p>";
				code1 += "<p>Name: " + data.category.name + "</p>";
				var i;
				var code = "<tr><th>Series_id</th><th>Name</th><th>f</th><th>Units</th><th>Updated</th></tr>";
				for(i in data.category.childseries){
					code += "<tr>";
					code += "<td>" + data.category.childseries[i].series_id + "</td>";
					code += "<td>" + data.category.childseries[i].name + "</td>";
					code += "<td>" + data.category.childseries[i].f + "</td>";
					code += "<td>" + data.category.childseries[i].units + "</td>";
					code += "<td>" + data.category.childseries[i].updated + "</td>";
					code += "</tr>";
				}
				document.getElementById("myLine").innerHTML = code1;
				document.getElementById("myTable").innerHTML = code;
			}
		$(document).ready(function() {
			$.getJSON("http://api.eia.gov/category/?api_key=8512eb9c1d86850c322cb9ed99052b73&category_id=2251679", processData);
		});
		break;


		case 'DC':
			function processData(data){
				var code1 = "<p>Category id: " + data.request.category_id + "</p>";
				code1 += "<p>Command: " + data.request.command + "</p>";
				code1 += "<p>Parent_category_id: " + data.category.parent_category_id + "</p>";
				code1 += "<p>Name: " + data.category.name + "</p>";
				var i;
				var code = "<tr><th>Series_id</th><th>Name</th><th>f</th><th>Units</th><th>Updated</th></tr>";
				for(i in data.category.childseries){
					code += "<tr>";
					code += "<td>" + data.category.childseries[i].series_id + "</td>";
					code += "<td>" + data.category.childseries[i].name + "</td>";
					code += "<td>" + data.category.childseries[i].f + "</td>";
					code += "<td>" + data.category.childseries[i].units + "</td>";
					code += "<td>" + data.category.childseries[i].updated + "</td>";
					code += "</tr>";
				}
				document.getElementById("myLine").innerHTML = code1;
				document.getElementById("myTable").innerHTML = code;
			}
		$(document).ready(function() {
			$.getJSON("http://api.eia.gov/category/?api_key=8512eb9c1d86850c322cb9ed99052b73&category_id=2251680", processData);
		});
		break;





		case 'FL':
			function processData(data){
				var code1 = "<p>Category id: " + data.request.category_id + "</p>";
				code1 += "<p>Command: " + data.request.command + "</p>";
				code1 += "<p>Parent_category_id: " + data.category.parent_category_id + "</p>";
				code1 += "<p>Name: " + data.category.name + "</p>";
				var i;
				var code = "<tr><th>Series_id</th><th>Name</th><th>f</th><th>Units</th><th>Updated</th></tr>";
				for(i in data.category.childseries){
					code += "<tr>";
					code += "<td>" + data.category.childseries[i].series_id + "</td>";
					code += "<td>" + data.category.childseries[i].name + "</td>";
					code += "<td>" + data.category.childseries[i].f + "</td>";
					code += "<td>" + data.category.childseries[i].units + "</td>";
					code += "<td>" + data.category.childseries[i].updated + "</td>";
					code += "</tr>";
				}
				document.getElementById("myLine").innerHTML = code1;
				document.getElementById("myTable").innerHTML = code;
			}
		$(document).ready(function() {
			$.getJSON("http://api.eia.gov/category/?api_key=8512eb9c1d86850c322cb9ed99052b73&category_id=2251681", processData);
		});
		break;



		case 'GA':
			function processData(data){
				var code1 = "<p>Category id: " + data.request.category_id + "</p>";
				code1 += "<p>Command: " + data.request.command + "</p>";
				code1 += "<p>Parent_category_id: " + data.category.parent_category_id + "</p>";
				code1 += "<p>Name: " + data.category.name + "</p>";
				var i;
				var code = "<tr><th>Series_id</th><th>Name</th><th>f</th><th>Units</th><th>Updated</th></tr>";
				for(i in data.category.childseries){
					code += "<tr>";
					code += "<td>" + data.category.childseries[i].series_id + "</td>";
					code += "<td>" + data.category.childseries[i].name + "</td>";
					code += "<td>" + data.category.childseries[i].f + "</td>";
					code += "<td>" + data.category.childseries[i].units + "</td>";
					code += "<td>" + data.category.childseries[i].updated + "</td>";
					code += "</tr>";
				}
				document.getElementById("myLine").innerHTML = code1;
				document.getElementById("myTable").innerHTML = code;
			}
		$(document).ready(function() {
			$.getJSON("http://api.eia.gov/category/?api_key=8512eb9c1d86850c322cb9ed99052b73&category_id=2251682", processData);
		});
		break;


		case 'HI':
			function processData(data){
				var code1 = "<p>Category id: " + data.request.category_id + "</p>";
				code1 += "<p>Command: " + data.request.command + "</p>";
				code1 += "<p>Parent_category_id: " + data.category.parent_category_id + "</p>";
				code1 += "<p>Name: " + data.category.name + "</p>";
				var i;
				var code = "<tr><th>Series_id</th><th>Name</th><th>f</th><th>Units</th><th>Updated</th></tr>";
				for(i in data.category.childseries){
					code += "<tr>";
					code += "<td>" + data.category.childseries[i].series_id + "</td>";
					code += "<td>" + data.category.childseries[i].name + "</td>";
					code += "<td>" + data.category.childseries[i].f + "</td>";
					code += "<td>" + data.category.childseries[i].units + "</td>";
					code += "<td>" + data.category.childseries[i].updated + "</td>";
					code += "</tr>";
				}
				document.getElementById("myLine").innerHTML = code1;
				document.getElementById("myTable").innerHTML = code;
			}
		$(document).ready(function() {
			$.getJSON("http://api.eia.gov/category/?api_key=8512eb9c1d86850c322cb9ed99052b73&category_id=2251683", processData);
		});
		break;



		case 'ID':
			function processData(data){
				var code1 = "<p>Category id: " + data.request.category_id + "</p>";
				code1 += "<p>Command: " + data.request.command + "</p>";
				code1 += "<p>Parent_category_id: " + data.category.parent_category_id + "</p>";
				code1 += "<p>Name: " + data.category.name + "</p>";
				var i;
				var code = "<tr><th>Series_id</th><th>Name</th><th>f</th><th>Units</th><th>Updated</th></tr>";
				for(i in data.category.childseries){
					code += "<tr>";
					code += "<td>" + data.category.childseries[i].series_id + "</td>";
					code += "<td>" + data.category.childseries[i].name + "</td>";
					code += "<td>" + data.category.childseries[i].f + "</td>";
					code += "<td>" + data.category.childseries[i].units + "</td>";
					code += "<td>" + data.category.childseries[i].updated + "</td>";
					code += "</tr>";
				}
				document.getElementById("myLine").innerHTML = code1;
				document.getElementById("myTable").innerHTML = code;
			}
		$(document).ready(function() {
			$.getJSON("http://api.eia.gov/category/?api_key=8512eb9c1d86850c322cb9ed99052b73&category_id=2251684", processData);
		});
		break;



		case 'IL':
			function processData(data){
				var code1 = "<p>Category id: " + data.request.category_id + "</p>";
				code1 += "<p>Command: " + data.request.command + "</p>";
				code1 += "<p>Parent_category_id: " + data.category.parent_category_id + "</p>";
				code1 += "<p>Name: " + data.category.name + "</p>";
				var i;
				var code = "<tr><th>Series_id</th><th>Name</th><th>f</th><th>Units</th><th>Updated</th></tr>";
				for(i in data.category.childseries){
					code += "<tr>";
					code += "<td>" + data.category.childseries[i].series_id + "</td>";
					code += "<td>" + data.category.childseries[i].name + "</td>";
					code += "<td>" + data.category.childseries[i].f + "</td>";
					code += "<td>" + data.category.childseries[i].units + "</td>";
					code += "<td>" + data.category.childseries[i].updated + "</td>";
					code += "</tr>";
				}
				document.getElementById("myLine").innerHTML = code1;
				document.getElementById("myTable").innerHTML = code;
			}
		$(document).ready(function() {
			$.getJSON("http://api.eia.gov/category/?api_key=8512eb9c1d86850c322cb9ed99052b73&category_id=2251685", processData);
		});
		break;



		case 'IN':
			function processData(data){
				var code1 = "<p>Category id: " + data.request.category_id + "</p>";
				code1 += "<p>Command: " + data.request.command + "</p>";
				code1 += "<p>Parent_category_id: " + data.category.parent_category_id + "</p>";
				code1 += "<p>Name: " + data.category.name + "</p>";
				var i;
				var code = "<tr><th>Series_id</th><th>Name</th><th>f</th><th>Units</th><th>Updated</th></tr>";
				for(i in data.category.childseries){
					code += "<tr>";
					code += "<td>" + data.category.childseries[i].series_id + "</td>";
					code += "<td>" + data.category.childseries[i].name + "</td>";
					code += "<td>" + data.category.childseries[i].f + "</td>";
					code += "<td>" + data.category.childseries[i].units + "</td>";
					code += "<td>" + data.category.childseries[i].updated + "</td>";
					code += "</tr>";
				}
				document.getElementById("myLine").innerHTML = code1;
				document.getElementById("myTable").innerHTML = code;
			}
		$(document).ready(function() {
			$.getJSON("http://api.eia.gov/category/?api_key=8512eb9c1d86850c322cb9ed99052b73&category_id=2251686", processData);
		});
		break;



		case 'IA':
			function processData(data){
				var code1 = "<p>Category id: " + data.request.category_id + "</p>";
				code1 += "<p>Command: " + data.request.command + "</p>";
				code1 += "<p>Parent_category_id: " + data.category.parent_category_id + "</p>";
				code1 += "<p>Name: " + data.category.name + "</p>";
				var i;
				var code = "<tr><th>Series_id</th><th>Name</th><th>f</th><th>Units</th><th>Updated</th></tr>";
				for(i in data.category.childseries){
					code += "<tr>";
					code += "<td>" + data.category.childseries[i].series_id + "</td>";
					code += "<td>" + data.category.childseries[i].name + "</td>";
					code += "<td>" + data.category.childseries[i].f + "</td>";
					code += "<td>" + data.category.childseries[i].units + "</td>";
					code += "<td>" + data.category.childseries[i].updated + "</td>";
					code += "</tr>";
				}
				document.getElementById("myLine").innerHTML = code1;
				document.getElementById("myTable").innerHTML = code;
			}
		$(document).ready(function() {
			$.getJSON("http://api.eia.gov/category/?api_key=8512eb9c1d86850c322cb9ed99052b73&category_id=2251687", processData);
		});
		break;



		case 'KS':
			function processData(data){
				var code1 = "<p>Category id: " + data.request.category_id + "</p>";
				code1 += "<p>Command: " + data.request.command + "</p>";
				code1 += "<p>Parent_category_id: " + data.category.parent_category_id + "</p>";
				code1 += "<p>Name: " + data.category.name + "</p>";
				var i;
				var code = "<tr><th>Series_id</th><th>Name</th><th>f</th><th>Units</th><th>Updated</th></tr>";
				for(i in data.category.childseries){
					code += "<tr>";
					code += "<td>" + data.category.childseries[i].series_id + "</td>";
					code += "<td>" + data.category.childseries[i].name + "</td>";
					code += "<td>" + data.category.childseries[i].f + "</td>";
					code += "<td>" + data.category.childseries[i].units + "</td>";
					code += "<td>" + data.category.childseries[i].updated + "</td>";
					code += "</tr>";
				}
				document.getElementById("myLine").innerHTML = code1;
				document.getElementById("myTable").innerHTML = code;
			}
		$(document).ready(function() {
			$.getJSON("http://api.eia.gov/category/?api_key=8512eb9c1d86850c322cb9ed99052b73&category_id=2251688", processData);
		});
		break;



		case 'KY':
			function processData(data){
				var code1 = "<p>Category id: " + data.request.category_id + "</p>";
				code1 += "<p>Command: " + data.request.command + "</p>";
				code1 += "<p>Parent_category_id: " + data.category.parent_category_id + "</p>";
				code1 += "<p>Name: " + data.category.name + "</p>";
				var i;
				var code = "<tr><th>Series_id</th><th>Name</th><th>f</th><th>Units</th><th>Updated</th></tr>";
				for(i in data.category.childseries){
					code += "<tr>";
					code += "<td>" + data.category.childseries[i].series_id + "</td>";
					code += "<td>" + data.category.childseries[i].name + "</td>";
					code += "<td>" + data.category.childseries[i].f + "</td>";
					code += "<td>" + data.category.childseries[i].units + "</td>";
					code += "<td>" + data.category.childseries[i].updated + "</td>";
					code += "</tr>";
				}
				document.getElementById("myLine").innerHTML = code1;
				document.getElementById("myTable").innerHTML = code;
			}
		$(document).ready(function() {
			$.getJSON("http://api.eia.gov/category/?api_key=8512eb9c1d86850c322cb9ed99052b73&category_id=2251689", processData);
		});
		break;



		case 'LA':
			function processData(data){
				var code1 = "<p>Category id: " + data.request.category_id + "</p>";
				code1 += "<p>Command: " + data.request.command + "</p>";
				code1 += "<p>Parent_category_id: " + data.category.parent_category_id + "</p>";
				code1 += "<p>Name: " + data.category.name + "</p>";
				var i;
				var code = "<tr><th>Series_id</th><th>Name</th><th>f</th><th>Units</th><th>Updated</th></tr>";
				for(i in data.category.childseries){
					code += "<tr>";
					code += "<td>" + data.category.childseries[i].series_id + "</td>";
					code += "<td>" + data.category.childseries[i].name + "</td>";
					code += "<td>" + data.category.childseries[i].f + "</td>";
					code += "<td>" + data.category.childseries[i].units + "</td>";
					code += "<td>" + data.category.childseries[i].updated + "</td>";
					code += "</tr>";
				}
				document.getElementById("myLine").innerHTML = code1;
				document.getElementById("myTable").innerHTML = code;
			}
		$(document).ready(function() {
			$.getJSON("http://api.eia.gov/category/?api_key=8512eb9c1d86850c322cb9ed99052b73&category_id=2251690", processData);
		});
		break;


		case 'ME':
			function processData(data){
				var code1 = "<p>Category id: " + data.request.category_id + "</p>";
				code1 += "<p>Command: " + data.request.command + "</p>";
				code1 += "<p>Parent_category_id: " + data.category.parent_category_id + "</p>";
				code1 += "<p>Name: " + data.category.name + "</p>";
				var i;
				var code = "<tr><th>Series_id</th><th>Name</th><th>f</th><th>Units</th><th>Updated</th></tr>";
				for(i in data.category.childseries){
					code += "<tr>";
					code += "<td>" + data.category.childseries[i].series_id + "</td>";
					code += "<td>" + data.category.childseries[i].name + "</td>";
					code += "<td>" + data.category.childseries[i].f + "</td>";
					code += "<td>" + data.category.childseries[i].units + "</td>";
					code += "<td>" + data.category.childseries[i].updated + "</td>";
					code += "</tr>";
				}
				document.getElementById("myLine").innerHTML = code1;
				document.getElementById("myTable").innerHTML = code;
			}
		$(document).ready(function() {
			$.getJSON("http://api.eia.gov/category/?api_key=8512eb9c1d86850c322cb9ed99052b73&category_id=2251691", processData);
		});
		break;


		case 'MD':
			function processData(data){
				var code1 = "<p>Category id: " + data.request.category_id + "</p>";
				code1 += "<p>Command: " + data.request.command + "</p>";
				code1 += "<p>Parent_category_id: " + data.category.parent_category_id + "</p>";
				code1 += "<p>Name: " + data.category.name + "</p>";
				var i;
				var code = "<tr><th>Series_id</th><th>Name</th><th>f</th><th>Units</th><th>Updated</th></tr>";
				for(i in data.category.childseries){
					code += "<tr>";
					code += "<td>" + data.category.childseries[i].series_id + "</td>";
					code += "<td>" + data.category.childseries[i].name + "</td>";
					code += "<td>" + data.category.childseries[i].f + "</td>";
					code += "<td>" + data.category.childseries[i].units + "</td>";
					code += "<td>" + data.category.childseries[i].updated + "</td>";
					code += "</tr>";
				}
				document.getElementById("myLine").innerHTML = code1;
				document.getElementById("myTable").innerHTML = code;
			}
		$(document).ready(function() {
			$.getJSON("http://api.eia.gov/category/?api_key=8512eb9c1d86850c322cb9ed99052b73&category_id=2251671", processData);
		});
		break;




		case 'MA':
			function processData(data){
				var code1 = "<p>Category id: " + data.request.category_id + "</p>";
				code1 += "<p>Command: " + data.request.command + "</p>";
				code1 += "<p>Parent_category_id: " + data.category.parent_category_id + "</p>";
				code1 += "<p>Name: " + data.category.name + "</p>";
				var i;
				var code = "<tr><th>Series_id</th><th>Name</th><th>f</th><th>Units</th><th>Updated</th></tr>";
				for(i in data.category.childseries){
					code += "<tr>";
					code += "<td>" + data.category.childseries[i].series_id + "</td>";
					code += "<td>" + data.category.childseries[i].name + "</td>";
					code += "<td>" + data.category.childseries[i].f + "</td>";
					code += "<td>" + data.category.childseries[i].units + "</td>";
					code += "<td>" + data.category.childseries[i].updated + "</td>";
					code += "</tr>";
				}
				document.getElementById("myLine").innerHTML = code1;
				document.getElementById("myTable").innerHTML = code;
			}
		$(document).ready(function() {
			$.getJSON("http://api.eia.gov/category/?api_key=8512eb9c1d86850c322cb9ed99052b73&category_id=2251692", processData);
		});
		break;



		case 'MI':
			function processData(data){
				var code1 = "<p>Category id: " + data.request.category_id + "</p>";
				code1 += "<p>Command: " + data.request.command + "</p>";
				code1 += "<p>Parent_category_id: " + data.category.parent_category_id + "</p>";
				code1 += "<p>Name: " + data.category.name + "</p>";
				var i;
				var code = "<tr><th>Series_id</th><th>Name</th><th>f</th><th>Units</th><th>Updated</th></tr>";
				for(i in data.category.childseries){
					code += "<tr>";
					code += "<td>" + data.category.childseries[i].series_id + "</td>";
					code += "<td>" + data.category.childseries[i].name + "</td>";
					code += "<td>" + data.category.childseries[i].f + "</td>";
					code += "<td>" + data.category.childseries[i].units + "</td>";
					code += "<td>" + data.category.childseries[i].updated + "</td>";
					code += "</tr>";
				}
				document.getElementById("myLine").innerHTML = code1;
				document.getElementById("myTable").innerHTML = code;
			}
		$(document).ready(function() {
			$.getJSON("http://api.eia.gov/category/?api_key=8512eb9c1d86850c322cb9ed99052b73&category_id=2251693", processData);
		});
		break;



		case 'MN':
			function processData(data){
				var code1 = "<p>Category id: " + data.request.category_id + "</p>";
				code1 += "<p>Command: " + data.request.command + "</p>";
				code1 += "<p>Parent_category_id: " + data.category.parent_category_id + "</p>";
				code1 += "<p>Name: " + data.category.name + "</p>";
				var i;
				var code = "<tr><th>Series_id</th><th>Name</th><th>f</th><th>Units</th><th>Updated</th></tr>";
				for(i in data.category.childseries){
					code += "<tr>";
					code += "<td>" + data.category.childseries[i].series_id + "</td>";
					code += "<td>" + data.category.childseries[i].name + "</td>";
					code += "<td>" + data.category.childseries[i].f + "</td>";
					code += "<td>" + data.category.childseries[i].units + "</td>";
					code += "<td>" + data.category.childseries[i].updated + "</td>";
					code += "</tr>";
				}
				document.getElementById("myLine").innerHTML = code1;
				document.getElementById("myTable").innerHTML = code;
			}
		$(document).ready(function() {
			$.getJSON("http://api.eia.gov/category/?api_key=8512eb9c1d86850c322cb9ed99052b73&category_id=2251694", processData);
		});
		break;


		case 'MS':
			function processData(data){
				var code1 = "<p>Category id: " + data.request.category_id + "</p>";
				code1 += "<p>Command: " + data.request.command + "</p>";
				code1 += "<p>Parent_category_id: " + data.category.parent_category_id + "</p>";
				code1 += "<p>Name: " + data.category.name + "</p>";
				var i;
				var code = "<tr><th>Series_id</th><th>Name</th><th>f</th><th>Units</th><th>Updated</th></tr>";
				for(i in data.category.childseries){
					code += "<tr>";
					code += "<td>" + data.category.childseries[i].series_id + "</td>";
					code += "<td>" + data.category.childseries[i].name + "</td>";
					code += "<td>" + data.category.childseries[i].f + "</td>";
					code += "<td>" + data.category.childseries[i].units + "</td>";
					code += "<td>" + data.category.childseries[i].updated + "</td>";
					code += "</tr>";
				}
				document.getElementById("myLine").innerHTML = code1;
				document.getElementById("myTable").innerHTML = code;
			}
		$(document).ready(function() {
			$.getJSON("http://api.eia.gov/category/?api_key=8512eb9c1d86850c322cb9ed99052b73&category_id=2251695", processData);
		});
		break;



		case 'MO':
			function processData(data){
				var code1 = "<p>Category id: " + data.request.category_id + "</p>";
				code1 += "<p>Command: " + data.request.command + "</p>";
				code1 += "<p>Parent_category_id: " + data.category.parent_category_id + "</p>";
				code1 += "<p>Name: " + data.category.name + "</p>";
				var i;
				var code = "<tr><th>Series_id</th><th>Name</th><th>f</th><th>Units</th><th>Updated</th></tr>";
				for(i in data.category.childseries){
					code += "<tr>";
					code += "<td>" + data.category.childseries[i].series_id + "</td>";
					code += "<td>" + data.category.childseries[i].name + "</td>";
					code += "<td>" + data.category.childseries[i].f + "</td>";
					code += "<td>" + data.category.childseries[i].units + "</td>";
					code += "<td>" + data.category.childseries[i].updated + "</td>";
					code += "</tr>";
				}
				document.getElementById("myLine").innerHTML = code1;
				document.getElementById("myTable").innerHTML = code;
			}
		$(document).ready(function() {
			$.getJSON("http://api.eia.gov/category/?api_key=8512eb9c1d86850c322cb9ed99052b73&category_id=2251696", processData);
		});
		break;



		case 'MT':
			function processData(data){
				var code1 = "<p>Category id: " + data.request.category_id + "</p>";
				code1 += "<p>Command: " + data.request.command + "</p>";
				code1 += "<p>Parent_category_id: " + data.category.parent_category_id + "</p>";
				code1 += "<p>Name: " + data.category.name + "</p>";
				var i;
				var code = "<tr><th>Series_id</th><th>Name</th><th>f</th><th>Units</th><th>Updated</th></tr>";
				for(i in data.category.childseries){
					code += "<tr>";
					code += "<td>" + data.category.childseries[i].series_id + "</td>";
					code += "<td>" + data.category.childseries[i].name + "</td>";
					code += "<td>" + data.category.childseries[i].f + "</td>";
					code += "<td>" + data.category.childseries[i].units + "</td>";
					code += "<td>" + data.category.childseries[i].updated + "</td>";
					code += "</tr>";
				}
				document.getElementById("myLine").innerHTML = code1;
				document.getElementById("myTable").innerHTML = code;
			}
		$(document).ready(function() {
			$.getJSON("http://api.eia.gov/category/?api_key=8512eb9c1d86850c322cb9ed99052b73&category_id=2251697", processData);
		});
		break;


		case 'NE':
			function processData(data){
				var code1 = "<p>Category id: " + data.request.category_id + "</p>";
				code1 += "<p>Command: " + data.request.command + "</p>";
				code1 += "<p>Parent_category_id: " + data.category.parent_category_id + "</p>";
				code1 += "<p>Name: " + data.category.name + "</p>";
				var i;
				var code = "<tr><th>Series_id</th><th>Name</th><th>f</th><th>Units</th><th>Updated</th></tr>";
				for(i in data.category.childseries){
					code += "<tr>";
					code += "<td>" + data.category.childseries[i].series_id + "</td>";
					code += "<td>" + data.category.childseries[i].name + "</td>";
					code += "<td>" + data.category.childseries[i].f + "</td>";
					code += "<td>" + data.category.childseries[i].units + "</td>";
					code += "<td>" + data.category.childseries[i].updated + "</td>";
					code += "</tr>";
				}
				document.getElementById("myLine").innerHTML = code1;
				document.getElementById("myTable").innerHTML = code;
			}
		$(document).ready(function() {
			$.getJSON("http://api.eia.gov/category/?api_key=8512eb9c1d86850c322cb9ed99052b73&category_id=2251698", processData);
		});
		break;



		case 'NV':
			function processData(data){
				var code1 = "<p>Category id: " + data.request.category_id + "</p>";
				code1 += "<p>Command: " + data.request.command + "</p>";
				code1 += "<p>Parent_category_id: " + data.category.parent_category_id + "</p>";
				code1 += "<p>Name: " + data.category.name + "</p>";
				var i;
				var code = "<tr><th>Series_id</th><th>Name</th><th>f</th><th>Units</th><th>Updated</th></tr>";
				for(i in data.category.childseries){
					code += "<tr>";
					code += "<td>" + data.category.childseries[i].series_id + "</td>";
					code += "<td>" + data.category.childseries[i].name + "</td>";
					code += "<td>" + data.category.childseries[i].f + "</td>";
					code += "<td>" + data.category.childseries[i].units + "</td>";
					code += "<td>" + data.category.childseries[i].updated + "</td>";
					code += "</tr>";
				}
				document.getElementById("myLine").innerHTML = code1;
				document.getElementById("myTable").innerHTML = code;
			}
		$(document).ready(function() {
			$.getJSON("http://api.eia.gov/category/?api_key=8512eb9c1d86850c322cb9ed99052b73&category_id=2251699", processData);
		});
		break;



		case 'NH':
			function processData(data){
				var code1 = "<p>Category id: " + data.request.category_id + "</p>";
				code1 += "<p>Command: " + data.request.command + "</p>";
				code1 += "<p>Parent_category_id: " + data.category.parent_category_id + "</p>";
				code1 += "<p>Name: " + data.category.name + "</p>";
				var i;
				var code = "<tr><th>Series_id</th><th>Name</th><th>f</th><th>Units</th><th>Updated</th></tr>";
				for(i in data.category.childseries){
					code += "<tr>";
					code += "<td>" + data.category.childseries[i].series_id + "</td>";
					code += "<td>" + data.category.childseries[i].name + "</td>";
					code += "<td>" + data.category.childseries[i].f + "</td>";
					code += "<td>" + data.category.childseries[i].units + "</td>";
					code += "<td>" + data.category.childseries[i].updated + "</td>";
					code += "</tr>";
				}
				document.getElementById("myLine").innerHTML = code1;
				document.getElementById("myTable").innerHTML = code;
			}
		$(document).ready(function() {
			$.getJSON("http://api.eia.gov/category/?api_key=8512eb9c1d86850c322cb9ed99052b73&category_id=2251700", processData);
		});
		break;



		case 'NJ':
			function processData(data){
				var code1 = "<p>Category id: " + data.request.category_id + "</p>";
				code1 += "<p>Command: " + data.request.command + "</p>";
				code1 += "<p>Parent_category_id: " + data.category.parent_category_id + "</p>";
				code1 += "<p>Name: " + data.category.name + "</p>";
				var i;
				var code = "<tr><th>Series_id</th><th>Name</th><th>f</th><th>Units</th><th>Updated</th></tr>";
				for(i in data.category.childseries){
					code += "<tr>";
					code += "<td>" + data.category.childseries[i].series_id + "</td>";
					code += "<td>" + data.category.childseries[i].name + "</td>";
					code += "<td>" + data.category.childseries[i].f + "</td>";
					code += "<td>" + data.category.childseries[i].units + "</td>";
					code += "<td>" + data.category.childseries[i].updated + "</td>";
					code += "</tr>";
				}
				document.getElementById("myLine").innerHTML = code1;
				document.getElementById("myTable").innerHTML = code;
			}
		$(document).ready(function() {
			$.getJSON("http://api.eia.gov/category/?api_key=8512eb9c1d86850c322cb9ed99052b73&category_id=2251701", processData);
		});
		break;


		case 'NM':
			function processData(data){
				var code1 = "<p>Category id: " + data.request.category_id + "</p>";
				code1 += "<p>Command: " + data.request.command + "</p>";
				code1 += "<p>Parent_category_id: " + data.category.parent_category_id + "</p>";
				code1 += "<p>Name: " + data.category.name + "</p>";
				var i;
				var code = "<tr><th>Series_id</th><th>Name</th><th>f</th><th>Units</th><th>Updated</th></tr>";
				for(i in data.category.childseries){
					code += "<tr>";
					code += "<td>" + data.category.childseries[i].series_id + "</td>";
					code += "<td>" + data.category.childseries[i].name + "</td>";
					code += "<td>" + data.category.childseries[i].f + "</td>";
					code += "<td>" + data.category.childseries[i].units + "</td>";
					code += "<td>" + data.category.childseries[i].updated + "</td>";
					code += "</tr>";
				}
				document.getElementById("myLine").innerHTML = code1;
				document.getElementById("myTable").innerHTML = code;
			}
		$(document).ready(function() {
			$.getJSON("http://api.eia.gov/category/?api_key=8512eb9c1d86850c322cb9ed99052b73&category_id=2251702", processData);
		});
		break;



		case 'NY':
			function processData(data){
				var code1 = "<p>Category id: " + data.request.category_id + "</p>";
				code1 += "<p>Command: " + data.request.command + "</p>";
				code1 += "<p>Parent_category_id: " + data.category.parent_category_id + "</p>";
				code1 += "<p>Name: " + data.category.name + "</p>";
				var i;
				var code = "<tr><th>Series_id</th><th>Name</th><th>f</th><th>Units</th><th>Updated</th></tr>";
				for(i in data.category.childseries){
					code += "<tr>";
					code += "<td>" + data.category.childseries[i].series_id + "</td>";
					code += "<td>" + data.category.childseries[i].name + "</td>";
					code += "<td>" + data.category.childseries[i].f + "</td>";
					code += "<td>" + data.category.childseries[i].units + "</td>";
					code += "<td>" + data.category.childseries[i].updated + "</td>";
					code += "</tr>";
				}
				document.getElementById("myLine").innerHTML = code1;
				document.getElementById("myTable").innerHTML = code;
			}
		$(document).ready(function() {
			$.getJSON("http://api.eia.gov/category/?api_key=8512eb9c1d86850c322cb9ed99052b73&category_id=2251703", processData);
		});
		break;



		case 'NC':
			function processData(data){
				var code1 = "<p>Category id: " + data.request.category_id + "</p>";
				code1 += "<p>Command: " + data.request.command + "</p>";
				code1 += "<p>Parent_category_id: " + data.category.parent_category_id + "</p>";
				code1 += "<p>Name: " + data.category.name + "</p>";
				var i;
				var code = "<tr><th>Series_id</th><th>Name</th><th>f</th><th>Units</th><th>Updated</th></tr>";
				for(i in data.category.childseries){
					code += "<tr>";
					code += "<td>" + data.category.childseries[i].series_id + "</td>";
					code += "<td>" + data.category.childseries[i].name + "</td>";
					code += "<td>" + data.category.childseries[i].f + "</td>";
					code += "<td>" + data.category.childseries[i].units + "</td>";
					code += "<td>" + data.category.childseries[i].updated + "</td>";
					code += "</tr>";
				}
				document.getElementById("myLine").innerHTML = code1;
				document.getElementById("myTable").innerHTML = code;
			}
		$(document).ready(function() {
			$.getJSON("http://api.eia.gov/category/?api_key=8512eb9c1d86850c322cb9ed99052b73&category_id=2251704", processData);
		});
		break;



		case 'ND':
			function processData(data){
				var code1 = "<p>Category id: " + data.request.category_id + "</p>";
				code1 += "<p>Command: " + data.request.command + "</p>";
				code1 += "<p>Parent_category_id: " + data.category.parent_category_id + "</p>";
				code1 += "<p>Name: " + data.category.name + "</p>";
				var i;
				var code = "<tr><th>Series_id</th><th>Name</th><th>f</th><th>Units</th><th>Updated</th></tr>";
				for(i in data.category.childseries){
					code += "<tr>";
					code += "<td>" + data.category.childseries[i].series_id + "</td>";
					code += "<td>" + data.category.childseries[i].name + "</td>";
					code += "<td>" + data.category.childseries[i].f + "</td>";
					code += "<td>" + data.category.childseries[i].units + "</td>";
					code += "<td>" + data.category.childseries[i].updated + "</td>";
					code += "</tr>";
				}
				document.getElementById("myLine").innerHTML = code1;
				document.getElementById("myTable").innerHTML = code;
			}
		$(document).ready(function() {
			$.getJSON("http://api.eia.gov/category/?api_key=8512eb9c1d86850c322cb9ed99052b73&category_id=2251705", processData);
		});
		break;

		case 'OH':
			function processData(data){
				var code1 = "<p>Category id: " + data.request.category_id + "</p>";
				code1 += "<p>Command: " + data.request.command + "</p>";
				code1 += "<p>Parent_category_id: " + data.category.parent_category_id + "</p>";
				code1 += "<p>Name: " + data.category.name + "</p>";
				var i;
				var code = "<tr><th>Series_id</th><th>Name</th><th>f</th><th>Units</th><th>Updated</th></tr>";
				for(i in data.category.childseries){
					code += "<tr>";
					code += "<td>" + data.category.childseries[i].series_id + "</td>";
					code += "<td>" + data.category.childseries[i].name + "</td>";
					code += "<td>" + data.category.childseries[i].f + "</td>";
					code += "<td>" + data.category.childseries[i].units + "</td>";
					code += "<td>" + data.category.childseries[i].updated + "</td>";
					code += "</tr>";
				}
				document.getElementById("myLine").innerHTML = code1;
				document.getElementById("myTable").innerHTML = code;
			}
		$(document).ready(function() {
			$.getJSON("http://api.eia.gov/category/?api_key=8512eb9c1d86850c322cb9ed99052b73&category_id=2251706", processData);
		});
		break;



		case 'OK':
			function processData(data){
				var code1 = "<p>Category id: " + data.request.category_id + "</p>";
				code1 += "<p>Command: " + data.request.command + "</p>";
				code1 += "<p>Parent_category_id: " + data.category.parent_category_id + "</p>";
				code1 += "<p>Name: " + data.category.name + "</p>";
				var i;
				var code = "<tr><th>Series_id</th><th>Name</th><th>f</th><th>Units</th><th>Updated</th></tr>";
				for(i in data.category.childseries){
					code += "<tr>";
					code += "<td>" + data.category.childseries[i].series_id + "</td>";
					code += "<td>" + data.category.childseries[i].name + "</td>";
					code += "<td>" + data.category.childseries[i].f + "</td>";
					code += "<td>" + data.category.childseries[i].units + "</td>";
					code += "<td>" + data.category.childseries[i].updated + "</td>";
					code += "</tr>";
				}
				document.getElementById("myLine").innerHTML = code1;
				document.getElementById("myTable").innerHTML = code;
			}
		$(document).ready(function() {
			$.getJSON("http://api.eia.gov/category/?api_key=8512eb9c1d86850c322cb9ed99052b73&category_id=2251707", processData);
		});
		break;




		case 'OR':
			function processData(data){
				var code1 = "<p>Category id: " + data.request.category_id + "</p>";
				code1 += "<p>Command: " + data.request.command + "</p>";
				code1 += "<p>Parent_category_id: " + data.category.parent_category_id + "</p>";
				code1 += "<p>Name: " + data.category.name + "</p>";
				var i;
				var code = "<tr><th>Series_id</th><th>Name</th><th>f</th><th>Units</th><th>Updated</th></tr>";
				for(i in data.category.childseries){
					code += "<tr>";
					code += "<td>" + data.category.childseries[i].series_id + "</td>";
					code += "<td>" + data.category.childseries[i].name + "</td>";
					code += "<td>" + data.category.childseries[i].f + "</td>";
					code += "<td>" + data.category.childseries[i].units + "</td>";
					code += "<td>" + data.category.childseries[i].updated + "</td>";
					code += "</tr>";
				}
				document.getElementById("myLine").innerHTML = code1;
				document.getElementById("myTable").innerHTML = code;
			}
		$(document).ready(function() {
			$.getJSON("http://api.eia.gov/category/?api_key=8512eb9c1d86850c322cb9ed99052b73&category_id=2251708", processData);
		});
		break;




		case 'PA':
			function processData(data){
				var code1 = "<p>Category id: " + data.request.category_id + "</p>";
				code1 += "<p>Command: " + data.request.command + "</p>";
				code1 += "<p>Parent_category_id: " + data.category.parent_category_id + "</p>";
				code1 += "<p>Name: " + data.category.name + "</p>";
				var i;
				var code = "<tr><th>Series_id</th><th>Name</th><th>f</th><th>Units</th><th>Updated</th></tr>";
				for(i in data.category.childseries){
					code += "<tr>";
					code += "<td>" + data.category.childseries[i].series_id + "</td>";
					code += "<td>" + data.category.childseries[i].name + "</td>";
					code += "<td>" + data.category.childseries[i].f + "</td>";
					code += "<td>" + data.category.childseries[i].units + "</td>";
					code += "<td>" + data.category.childseries[i].updated + "</td>";
					code += "</tr>";
				}
				document.getElementById("myLine").innerHTML = code1;
				document.getElementById("myTable").innerHTML = code;
			}
		$(document).ready(function() {
			$.getJSON("http://api.eia.gov/category/?api_key=8512eb9c1d86850c322cb9ed99052b73&category_id=2251709", processData);
		});
		break;




		case 'RI':
			function processData(data){
				var code1 = "<p>Category id: " + data.request.category_id + "</p>";
				code1 += "<p>Command: " + data.request.command + "</p>";
				code1 += "<p>Parent_category_id: " + data.category.parent_category_id + "</p>";
				code1 += "<p>Name: " + data.category.name + "</p>";
				var i;
				var code = "<tr><th>Series_id</th><th>Name</th><th>f</th><th>Units</th><th>Updated</th></tr>";
				for(i in data.category.childseries){
					code += "<tr>";
					code += "<td>" + data.category.childseries[i].series_id + "</td>";
					code += "<td>" + data.category.childseries[i].name + "</td>";
					code += "<td>" + data.category.childseries[i].f + "</td>";
					code += "<td>" + data.category.childseries[i].units + "</td>";
					code += "<td>" + data.category.childseries[i].updated + "</td>";
					code += "</tr>";
				}
				document.getElementById("myLine").innerHTML = code1;
				document.getElementById("myTable").innerHTML = code;
			}
		$(document).ready(function() {
			$.getJSON("http://api.eia.gov/category/?api_key=8512eb9c1d86850c322cb9ed99052b73&category_id=2251710", processData);
		});
		break;




		case 'RI':
			function processData(data){
				var code1 = "<p>Category id: " + data.request.category_id + "</p>";
				code1 += "<p>Command: " + data.request.command + "</p>";
				code1 += "<p>Parent_category_id: " + data.category.parent_category_id + "</p>";
				code1 += "<p>Name: " + data.category.name + "</p>";
				var i;
				var code = "<tr><th>Series_id</th><th>Name</th><th>f</th><th>Units</th><th>Updated</th></tr>";
				for(i in data.category.childseries){
					code += "<tr>";
					code += "<td>" + data.category.childseries[i].series_id + "</td>";
					code += "<td>" + data.category.childseries[i].name + "</td>";
					code += "<td>" + data.category.childseries[i].f + "</td>";
					code += "<td>" + data.category.childseries[i].units + "</td>";
					code += "<td>" + data.category.childseries[i].updated + "</td>";
					code += "</tr>";
				}
				document.getElementById("myLine").innerHTML = code1;
				document.getElementById("myTable").innerHTML = code;
			}
		$(document).ready(function() {
			$.getJSON("http://api.eia.gov/category/?api_key=8512eb9c1d86850c322cb9ed99052b73&category_id=2251710", processData);
		});
		break;


		case 'SC':
			function processData(data){
				var code1 = "<p>Category id: " + data.request.category_id + "</p>";
				code1 += "<p>Command: " + data.request.command + "</p>";
				code1 += "<p>Parent_category_id: " + data.category.parent_category_id + "</p>";
				code1 += "<p>Name: " + data.category.name + "</p>";
				var i;
				var code = "<tr><th>Series_id</th><th>Name</th><th>f</th><th>Units</th><th>Updated</th></tr>";
				for(i in data.category.childseries){
					code += "<tr>";
					code += "<td>" + data.category.childseries[i].series_id + "</td>";
					code += "<td>" + data.category.childseries[i].name + "</td>";
					code += "<td>" + data.category.childseries[i].f + "</td>";
					code += "<td>" + data.category.childseries[i].units + "</td>";
					code += "<td>" + data.category.childseries[i].updated + "</td>";
					code += "</tr>";
				}
				document.getElementById("myLine").innerHTML = code1;
				document.getElementById("myTable").innerHTML = code;
			}
		$(document).ready(function() {
			$.getJSON("http://api.eia.gov/category/?api_key=8512eb9c1d86850c322cb9ed99052b73&category_id=2251711", processData);
		});
		break;



		case 'SD':
			function processData(data){
				var code1 = "<p>Category id: " + data.request.category_id + "</p>";
				code1 += "<p>Command: " + data.request.command + "</p>";
				code1 += "<p>Parent_category_id: " + data.category.parent_category_id + "</p>";
				code1 += "<p>Name: " + data.category.name + "</p>";
				var i;
				var code = "<tr><th>Series_id</th><th>Name</th><th>f</th><th>Units</th><th>Updated</th></tr>";
				for(i in data.category.childseries){
					code += "<tr>";
					code += "<td>" + data.category.childseries[i].series_id + "</td>";
					code += "<td>" + data.category.childseries[i].name + "</td>";
					code += "<td>" + data.category.childseries[i].f + "</td>";
					code += "<td>" + data.category.childseries[i].units + "</td>";
					code += "<td>" + data.category.childseries[i].updated + "</td>";
					code += "</tr>";
				}
				document.getElementById("myLine").innerHTML = code1;
				document.getElementById("myTable").innerHTML = code;
			}
		$(document).ready(function() {
			$.getJSON("http://api.eia.gov/category/?api_key=8512eb9c1d86850c322cb9ed99052b73&category_id=2251712", processData);
		});
		break;



		case 'TN':
			function processData(data){
				var code1 = "<p>Category id: " + data.request.category_id + "</p>";
				code1 += "<p>Command: " + data.request.command + "</p>";
				code1 += "<p>Parent_category_id: " + data.category.parent_category_id + "</p>";
				code1 += "<p>Name: " + data.category.name + "</p>";
				var i;
				var code = "<tr><th>Series_id</th><th>Name</th><th>f</th><th>Units</th><th>Updated</th></tr>";
				for(i in data.category.childseries){
					code += "<tr>";
					code += "<td>" + data.category.childseries[i].series_id + "</td>";
					code += "<td>" + data.category.childseries[i].name + "</td>";
					code += "<td>" + data.category.childseries[i].f + "</td>";
					code += "<td>" + data.category.childseries[i].units + "</td>";
					code += "<td>" + data.category.childseries[i].updated + "</td>";
					code += "</tr>";
				}
				document.getElementById("myLine").innerHTML = code1;
				document.getElementById("myTable").innerHTML = code;
			}
		$(document).ready(function() {
			$.getJSON("http://api.eia.gov/category/?api_key=8512eb9c1d86850c322cb9ed99052b73&category_id=2251713", processData);
		});
		break;



		case 'TX':
			function processData(data){
				var code1 = "<p>Category id: " + data.request.category_id + "</p>";
				code1 += "<p>Command: " + data.request.command + "</p>";
				code1 += "<p>Parent_category_id: " + data.category.parent_category_id + "</p>";
				code1 += "<p>Name: " + data.category.name + "</p>";
				var i;
				var code = "<tr><th>Series_id</th><th>Name</th><th>f</th><th>Units</th><th>Updated</th></tr>";
				for(i in data.category.childseries){
					code += "<tr>";
					code += "<td>" + data.category.childseries[i].series_id + "</td>";
					code += "<td>" + data.category.childseries[i].name + "</td>";
					code += "<td>" + data.category.childseries[i].f + "</td>";
					code += "<td>" + data.category.childseries[i].units + "</td>";
					code += "<td>" + data.category.childseries[i].updated + "</td>";
					code += "</tr>";
				}
				document.getElementById("myLine").innerHTML = code1;
				document.getElementById("myTable").innerHTML = code;
			}
		$(document).ready(function() {
			$.getJSON("http://api.eia.gov/category/?api_key=8512eb9c1d86850c322cb9ed99052b73&category_id=2251714", processData);
		});
		break;



		case 'US':
			function processData(data){
				var code1 = "<p>Category id: " + data.request.category_id + "</p>";
				code1 += "<p>Command: " + data.request.command + "</p>";
				code1 += "<p>Parent_category_id: " + data.category.parent_category_id + "</p>";
				code1 += "<p>Name: " + data.category.name + "</p>";
				var i;
				var code = "<tr><th>Series_id</th><th>Name</th><th>f</th><th>Units</th><th>Updated</th></tr>";
				for(i in data.category.childseries){
					code += "<tr>";
					code += "<td>" + data.category.childseries[i].series_id + "</td>";
					code += "<td>" + data.category.childseries[i].name + "</td>";
					code += "<td>" + data.category.childseries[i].f + "</td>";
					code += "<td>" + data.category.childseries[i].units + "</td>";
					code += "<td>" + data.category.childseries[i].updated + "</td>";
					code += "</tr>";
				}
				document.getElementById("myLine").innerHTML = code1;
				document.getElementById("myTable").innerHTML = code;
			}
		$(document).ready(function() {
			$.getJSON("http://api.eia.gov/category/?api_key=8512eb9c1d86850c322cb9ed99052b73&category_id=2251715", processData);
		});
		break;


		case 'UT':
			function processData(data){
				var code1 = "<p>Category id: " + data.request.category_id + "</p>";
				code1 += "<p>Command: " + data.request.command + "</p>";
				code1 += "<p>Parent_category_id: " + data.category.parent_category_id + "</p>";
				code1 += "<p>Name: " + data.category.name + "</p>";
				var i;
				var code = "<tr><th>Series_id</th><th>Name</th><th>f</th><th>Units</th><th>Updated</th></tr>";
				for(i in data.category.childseries){
					code += "<tr>";
					code += "<td>" + data.category.childseries[i].series_id + "</td>";
					code += "<td>" + data.category.childseries[i].name + "</td>";
					code += "<td>" + data.category.childseries[i].f + "</td>";
					code += "<td>" + data.category.childseries[i].units + "</td>";
					code += "<td>" + data.category.childseries[i].updated + "</td>";
					code += "</tr>";
				}
				document.getElementById("myLine").innerHTML = code1;
				document.getElementById("myTable").innerHTML = code;
			}
		$(document).ready(function() {
			$.getJSON("http://api.eia.gov/category/?api_key=8512eb9c1d86850c322cb9ed99052b73&category_id=2251716", processData);
		});
		break;



		case 'VT':
			function processData(data){
				var code1 = "<p>Category id: " + data.request.category_id + "</p>";
				code1 += "<p>Command: " + data.request.command + "</p>";
				code1 += "<p>Parent_category_id: " + data.category.parent_category_id + "</p>";
				code1 += "<p>Name: " + data.category.name + "</p>";
				var i;
				var code = "<tr><th>Series_id</th><th>Name</th><th>f</th><th>Units</th><th>Updated</th></tr>";
				for(i in data.category.childseries){
					code += "<tr>";
					code += "<td>" + data.category.childseries[i].series_id + "</td>";
					code += "<td>" + data.category.childseries[i].name + "</td>";
					code += "<td>" + data.category.childseries[i].f + "</td>";
					code += "<td>" + data.category.childseries[i].units + "</td>";
					code += "<td>" + data.category.childseries[i].updated + "</td>";
					code += "</tr>";
				}
				document.getElementById("myLine").innerHTML = code1;
				document.getElementById("myTable").innerHTML = code;
			}
		$(document).ready(function() {
			$.getJSON("http://api.eia.gov/category/?api_key=8512eb9c1d86850c322cb9ed99052b73&category_id=2251717", processData);
		});
		break;


		case 'VA':
			function processData(data){
				var code1 = "<p>Category id: " + data.request.category_id + "</p>";
				code1 += "<p>Command: " + data.request.command + "</p>";
				code1 += "<p>Parent_category_id: " + data.category.parent_category_id + "</p>";
				code1 += "<p>Name: " + data.category.name + "</p>";
				var i;
				var code = "<tr><th>Series_id</th><th>Name</th><th>f</th><th>Units</th><th>Updated</th></tr>";
				for(i in data.category.childseries){
					code += "<tr>";
					code += "<td>" + data.category.childseries[i].series_id + "</td>";
					code += "<td>" + data.category.childseries[i].name + "</td>";
					code += "<td>" + data.category.childseries[i].f + "</td>";
					code += "<td>" + data.category.childseries[i].units + "</td>";
					code += "<td>" + data.category.childseries[i].updated + "</td>";
					code += "</tr>";
				}
				document.getElementById("myLine").innerHTML = code1;
				document.getElementById("myTable").innerHTML = code;
			}
		$(document).ready(function() {
			$.getJSON("http://api.eia.gov/category/?api_key=8512eb9c1d86850c322cb9ed99052b73&category_id=2251718", processData);
		});
		break;



		case 'WA':
			function processData(data){
				var code1 = "<p>Category id: " + data.request.category_id + "</p>";
				code1 += "<p>Command: " + data.request.command + "</p>";
				code1 += "<p>Parent_category_id: " + data.category.parent_category_id + "</p>";
				code1 += "<p>Name: " + data.category.name + "</p>";
				var i;
				var code = "<tr><th>Series_id</th><th>Name</th><th>f</th><th>Units</th><th>Updated</th></tr>";
				for(i in data.category.childseries){
					code += "<tr>";
					code += "<td>" + data.category.childseries[i].series_id + "</td>";
					code += "<td>" + data.category.childseries[i].name + "</td>";
					code += "<td>" + data.category.childseries[i].f + "</td>";
					code += "<td>" + data.category.childseries[i].units + "</td>";
					code += "<td>" + data.category.childseries[i].updated + "</td>";
					code += "</tr>";
				}
				document.getElementById("myLine").innerHTML = code1;
				document.getElementById("myTable").innerHTML = code;
			}
		$(document).ready(function() {
			$.getJSON("http://api.eia.gov/category/?api_key=8512eb9c1d86850c322cb9ed99052b73&category_id=2251719", processData);
		});
		break;


		case 'WV':
			function processData(data){
				var code1 = "<p>Category id: " + data.request.category_id + "</p>";
				code1 += "<p>Command: " + data.request.command + "</p>";
				code1 += "<p>Parent_category_id: " + data.category.parent_category_id + "</p>";
				code1 += "<p>Name: " + data.category.name + "</p>";
				var i;
				var code = "<tr><th>Series_id</th><th>Name</th><th>f</th><th>Units</th><th>Updated</th></tr>";
				for(i in data.category.childseries){
					code += "<tr>";
					code += "<td>" + data.category.childseries[i].series_id + "</td>";
					code += "<td>" + data.category.childseries[i].name + "</td>";
					code += "<td>" + data.category.childseries[i].f + "</td>";
					code += "<td>" + data.category.childseries[i].units + "</td>";
					code += "<td>" + data.category.childseries[i].updated + "</td>";
					code += "</tr>";
				}
				document.getElementById("myLine").innerHTML = code1;
				document.getElementById("myTable").innerHTML = code;
			}
		$(document).ready(function() {
			$.getJSON("http://api.eia.gov/category/?api_key=8512eb9c1d86850c322cb9ed99052b73&category_id=2251720", processData);
		});
		break;



		case 'WI':
			function processData(data){
				var code1 = "<p>Category id: " + data.request.category_id + "</p>";
				code1 += "<p>Command: " + data.request.command + "</p>";
				code1 += "<p>Parent_category_id: " + data.category.parent_category_id + "</p>";
				code1 += "<p>Name: " + data.category.name + "</p>";
				var i;
				var code = "<tr><th>Series_id</th><th>Name</th><th>f</th><th>Units</th><th>Updated</th></tr>";
				for(i in data.category.childseries){
					code += "<tr>";
					code += "<td>" + data.category.childseries[i].series_id + "</td>";
					code += "<td>" + data.category.childseries[i].name + "</td>";
					code += "<td>" + data.category.childseries[i].f + "</td>";
					code += "<td>" + data.category.childseries[i].units + "</td>";
					code += "<td>" + data.category.childseries[i].updated + "</td>";
					code += "</tr>";
				}
				document.getElementById("myLine").innerHTML = code1;
				document.getElementById("myTable").innerHTML = code;
			}
		$(document).ready(function() {
			$.getJSON("http://api.eia.gov/category/?api_key=8512eb9c1d86850c322cb9ed99052b73&category_id=2251721", processData);
		});
		break;



		case 'WY':
			function processData(data){
				var code1 = "<p>Category id: " + data.request.category_id + "</p>";
				code1 += "<p>Command: " + data.request.command + "</p>";
				code1 += "<p>Parent_category_id: " + data.category.parent_category_id + "</p>";
				code1 += "<p>Name: " + data.category.name + "</p>";
				var i;
				var code = "<tr><th>Series_id</th><th>Name</th><th>f</th><th>Units</th><th>Updated</th></tr>";
				for(i in data.category.childseries){
					code += "<tr>";
					code += "<td>" + data.category.childseries[i].series_id + "</td>";
					code += "<td>" + data.category.childseries[i].name + "</td>";
					code += "<td>" + data.category.childseries[i].f + "</td>";
					code += "<td>" + data.category.childseries[i].units + "</td>";
					code += "<td>" + data.category.childseries[i].updated + "</td>";
					code += "</tr>";
				}
				document.getElementById("myLine").innerHTML = code1;
				document.getElementById("myTable").innerHTML = code;
			}
		$(document).ready(function() {
			$.getJSON("http://api.eia.gov/category/?api_key=8512eb9c1d86850c322cb9ed99052b73&category_id=2251722", processData);
		});
		break;
	}
}
